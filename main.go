package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/fredsar/adventure/internal/models"
	"gitlab.com/fredsar/adventure/internal/transport"
)

func main() {
	story, err := initializeApplication()
	if err != nil {
		log.Panic(err)
	}
	h := transport.NewHandler(story, "internal/templates/format.html")
	log.Println("server starting at 8080")
	log.Fatal(http.ListenAndServe(":8080", h))
}

func initializeApplication() (models.Story, error) {
	fileName := parseFlags()

	data, err := parseFile(fileName)
	if err != nil {
		log.Panic(err)
	}

	return parseJSONToStory(data)
}

func parseFlags() string {
	fileName := flag.String("file", "gopher.json", "The file name that will hold the whole story.")
	flag.Parse()

	return *fileName
}

func parseJSONToStory(data []byte) (models.Story, error) {
	var story models.Story
	if err := json.Unmarshal(data, &story); err != nil {
		return models.Story{}, err
	}

	return story, nil
}

func parseFile(path string) ([]byte, error) {
	fileBytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	return fileBytes, nil
}
