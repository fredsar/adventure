package transport

import (
	"html/template"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/fredsar/adventure/internal/models"
)

func NewHandler(s models.Story, tp string) Handler {
	return Handler{
		story:        s,
		templatePath: tp,
	}
}

type Handler struct {
	story        models.Story
	templatePath string
}

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) { //nolint: varnamelen
	data, err := ioutil.ReadFile(h.templatePath)
	if err != nil {
		http.Error(w, "something went wrong reading the template", http.StatusInternalServerError)

		return
	}
	tmpl := template.Must(template.New("").Parse(string(data)))

	path := r.URL.Path
	if path == "" || path == "/" {
		path = "/intro"
	}
	path = path[1:]

	chapter, ok := h.story[path]
	if ok {
		err = tmpl.Execute(w, chapter)
		if err != nil {
			log.Print(err)
			http.Error(w, "something went wrong 1", http.StatusInternalServerError)
		}

		return
	}
	http.Error(w, "Chapter not found", http.StatusNotFound)
}
