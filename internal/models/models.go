package models

// Story the struct that will hold the title-contect relationship.
type Story map[string]Chapter

// Chapter is the struct that contains the chapter information.
type Chapter struct {
	Title     string   `json:"title"`
	Paragraph []string `json:"story"`
	Options   []Option `json:"options"`
}

// Option is the struct that contains the options to point at.
type Option struct {
	Text string `json:"text"`
	Arc  string `json:"arc"`
}
